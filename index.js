const express = require('express');
const request = require('request');
const CronJob = require('cron').CronJob;
const app = express();
const port = parseInt(process.env.PORT, 10) || 8000;

const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 4002 });
var ws = null;

const nodered_url = "https://saplabendpointunsecure.cfapps.eu10.hana.ondemand.com/importShot";
const username = "SAP_NOW";
const passw = "!Vimercate2015!";

var job = null;

function _coinFlip() {
    return (Math.floor(Math.random() * 2) == 0);
}

function _getRandomFloat(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

wss.on('connection', function connection(webs) {
    ws = webs;
});

app.get('/', function(req, res) {
    var numberOfShots = Math.floor(Math.random() * 20),
        chiamate = 0,
        canestri = 0,
        canestriDaTre = 0,
        socketMessage = {
            shotNr: -1,
            isCanestro: false,
            isThreePoints: false,
            x: -1,
            y: -1,
            message: ""
        };
    console.log("Devo fare " + numberOfShots + " tiri");
    if (job){
        job.stop();
    }

    var shotNr = 0,
        shots = [];
    
    job = new CronJob(
            '*/5 * * * * *', 
            async function() {
                if (numberOfShots > 0){

                    var isGoal = _coinFlip(),
                        isThreePoints = _coinFlip(),
                        newShot = {
                            x: _getRandomFloat(70,430),
                            y: _getRandomFloat(70,430),
                            z: _getRandomFloat(1,10),
                            threePoints: isThreePoints
                        },
            
                        options = {
                            method: 'post',
                            body: newShot,
                            json: true,
                            url: nodered_url,
                        };
                    
                    shotNr++;

                    socketMessage.shotNr = shotNr;
                    // socketMessage.isCanestro = isGoal;
                    socketMessage.isThreePoints = isThreePoints;
                    socketMessage.x = newShot.x;
                    socketMessage.y = newShot.y;
                    socketMessage.message = shotNr + " -- x: " + newShot.x + " y: " + newShot.y;
                    
                    shots.push(JSON.parse(JSON.stringify(socketMessage)));

                    // creazione tiro
                    console.log(shotNr + " -- x: " + newShot.x + " y: " + newShot.y);
                    try{
                        await request(options);
                    }
                    catch(err){
                        console.log(err);
                    }
                    chiamate++;
                    if (ws){
                        console.log("Mando messaggio a ws");
                        ws.send(JSON.stringify(shots));
                    }
                    console.log(numberOfShots + " shots remaining");
                    
                    if (isGoal){
                        setTimeout(
                            async function(){
                                // il tiro è andato in canestro
                                options = {
                                    method: 'put',
                                    json: true,
                                    url: nodered_url,
                                };
                                
                                await request(options);
                                chiamate++;
                                if (isThreePoints){
                                    canestriDaTre++;
                                }
                                else{
                                    canestri++;
                                }

                                socketMessage.shotNr = shotNr;
                                // socketMessage.isCanestro = isGoal;
                                socketMessage.isThreePoints = isThreePoints;
                                socketMessage.x = newShot.x;
                                socketMessage.y = newShot.y;
                                socketMessage.message = shotNr + " -- x: " + newShot.x + " y: " + newShot.y;
                                
                                shots.push(JSON.parse(JSON.stringify(socketMessage)));

                                console.log("Canestro!");
                                
                                if (ws){
                                    console.log("Mando messaggio a ws");
                                    ws.send(JSON.stringify(shots));
                                }
                            }.bind(this),
                            1000
                        );
                    }
                    numberOfShots--;
                } // while
                else{
                    console.log("-- Ho finito! --");
                    console.log("Chiamate: " + chiamate);
                    console.log("Canestri fatti: " + canestri);
                    console.log("Canestri da tre: " + canestriDaTre);
                    console.log("----------------");
                    shots = [];
                    job.stop();
                }
            }, 
            null,
            true, 
            'America/Los_Angeles'
        );
    res.status(200).send("Faccio " + numberOfShots + " tiri");
});

app.listen(port, () => console.log(`SAP Basket simulator app listening on port ${port}!`));
